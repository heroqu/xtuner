<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim rdb_SQLSrvAuth As System.Windows.Forms.RadioButton
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.TabControl_Main = New System.Windows.Forms.TabControl
        Me.tbp_DB = New System.Windows.Forms.TabPage
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel
        Me.Label17 = New System.Windows.Forms.Label
        Me.Label19 = New System.Windows.Forms.Label
        Me.txb_Server = New System.Windows.Forms.TextBox
        Me.Label18 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.rdb_WindowsAuthentication = New System.Windows.Forms.RadioButton
        Me.txb_DataBase = New System.Windows.Forms.TextBox
        Me.txb_ServerUser = New System.Windows.Forms.TextBox
        Me.txb_ServerPassword = New System.Windows.Forms.TextBox
        Me.Label16 = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.Label20 = New System.Windows.Forms.Label
        Me.txb_ConnStr = New System.Windows.Forms.TextBox
        Me.btn_ConnStrBuild = New System.Windows.Forms.Button
        Me.btn_ConnTest = New System.Windows.Forms.Button
        Me.tbp_Project = New System.Windows.Forms.TabPage
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.Label14 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.lbl_WorkDir = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.txb_GUID = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.txb_ProjectName = New System.Windows.Forms.TextBox
        Me.txb_ProjectDescpition = New System.Windows.Forms.TextBox
        Me.txb_CreatedAt = New System.Windows.Forms.TextBox
        Me.txb_UpdatedAt = New System.Windows.Forms.TextBox
        Me.tbp_Tables = New System.Windows.Forms.TabPage
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
        Me.SplitContainer3 = New System.Windows.Forms.SplitContainer
        Me.dgv_Tables = New System.Windows.Forms.DataGridView
        Me.Label11 = New System.Windows.Forms.Label
        Me.txb_TableCondition = New System.Windows.Forms.TextBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.dgv_Columns = New System.Windows.Forms.DataGridView
        Me.Label13 = New System.Windows.Forms.Label
        Me.ckb_ShowExtraColumns = New System.Windows.Forms.CheckBox
        Me.tbp_Dump = New System.Windows.Forms.TabPage
        Me.TableLayoutPanel4 = New System.Windows.Forms.TableLayoutPanel
        Me.Label25 = New System.Windows.Forms.Label
        Me.Label22 = New System.Windows.Forms.Label
        Me.txb_SQL = New System.Windows.Forms.TextBox
        Me.Label21 = New System.Windows.Forms.Label
        Me.lbl_ProjectFile = New System.Windows.Forms.Label
        Me.txb_DumpFile = New System.Windows.Forms.TextBox
        Me.btn_DumpFileChoose = New System.Windows.Forms.Button
        Me.ToolStrip3 = New System.Windows.Forms.ToolStrip
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.tsb_Dump = New System.Windows.Forms.ToolStripButton
        Me.ImageList_Dump = New System.Windows.Forms.ImageList(Me.components)
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.SplitContainer2 = New System.Windows.Forms.SplitContainer
        Me.TableLayoutPanel5 = New System.Windows.Forms.TableLayoutPanel
        Me.TableLayoutPanel6 = New System.Windows.Forms.TableLayoutPanel
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.TextBox3 = New System.Windows.Forms.TextBox
        Me.TextBox4 = New System.Windows.Forms.TextBox
        Me.TextBox5 = New System.Windows.Forms.TextBox
        Me.TabPage4 = New System.Windows.Forms.TabPage
        Me.TabPage5 = New System.Windows.Forms.TabPage
        Me.TableLayoutPanel7 = New System.Windows.Forms.TableLayoutPanel
        Me.DataGridView1 = New System.Windows.Forms.DataGridView
        Me.newToolStripButton = New System.Windows.Forms.ToolStripButton
        Me.openToolStripButton = New System.Windows.Forms.ToolStripButton
        Me.saveToolStripButton = New System.Windows.Forms.ToolStripButton
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip
        Me.tsb_New = New System.Windows.Forms.ToolStripButton
        Me.tsb_Open = New System.Windows.Forms.ToolStripButton
        Me.tsb_Save = New System.Windows.Forms.ToolStripButton
        rdb_SQLSrvAuth = New System.Windows.Forms.RadioButton
        Me.TabControl_Main.SuspendLayout()
        Me.tbp_DB.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.tbp_Project.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.tbp_Tables.SuspendLayout()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.SplitContainer3.Panel1.SuspendLayout()
        Me.SplitContainer3.Panel2.SuspendLayout()
        Me.SplitContainer3.SuspendLayout()
        CType(Me.dgv_Tables, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgv_Columns, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbp_Dump.SuspendLayout()
        Me.TableLayoutPanel4.SuspendLayout()
        Me.ToolStrip3.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.SplitContainer2.Panel1.SuspendLayout()
        Me.SplitContainer2.Panel2.SuspendLayout()
        Me.SplitContainer2.SuspendLayout()
        Me.TableLayoutPanel6.SuspendLayout()
        Me.TabPage5.SuspendLayout()
        Me.TableLayoutPanel7.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ToolStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'rdb_SQLSrvAuth
        '
        rdb_SQLSrvAuth.AutoSize = True
        rdb_SQLSrvAuth.Checked = True
        rdb_SQLSrvAuth.Location = New System.Drawing.Point(6, 40)
        rdb_SQLSrvAuth.Name = "rdb_SQLSrvAuth"
        rdb_SQLSrvAuth.Size = New System.Drawing.Size(154, 17)
        rdb_SQLSrvAuth.TabIndex = 14
        rdb_SQLSrvAuth.TabStop = True
        rdb_SQLSrvAuth.Text = "SQL Server  Authentication"
        rdb_SQLSrvAuth.UseVisualStyleBackColor = True
        '
        'TabControl_Main
        '
        Me.TabControl_Main.Controls.Add(Me.tbp_DB)
        Me.TabControl_Main.Controls.Add(Me.tbp_Project)
        Me.TabControl_Main.Controls.Add(Me.tbp_Tables)
        Me.TabControl_Main.Controls.Add(Me.tbp_Dump)
        Me.TabControl_Main.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl_Main.Location = New System.Drawing.Point(0, 25)
        Me.TabControl_Main.Name = "TabControl_Main"
        Me.TabControl_Main.SelectedIndex = 0
        Me.TabControl_Main.Size = New System.Drawing.Size(734, 419)
        Me.TabControl_Main.TabIndex = 1
        '
        'tbp_DB
        '
        Me.tbp_DB.Controls.Add(Me.TableLayoutPanel2)
        Me.tbp_DB.Location = New System.Drawing.Point(4, 22)
        Me.tbp_DB.Name = "tbp_DB"
        Me.tbp_DB.Padding = New System.Windows.Forms.Padding(3)
        Me.tbp_DB.Size = New System.Drawing.Size(726, 393)
        Me.tbp_DB.TabIndex = 2
        Me.tbp_DB.Text = "База данных"
        Me.tbp_DB.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 5
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 137.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 9.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 61.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 166.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.Label17, 0, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.Label19, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.txb_Server, 2, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.Label18, 0, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.GroupBox1, 2, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.txb_DataBase, 2, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.txb_ServerUser, 3, 3)
        Me.TableLayoutPanel2.Controls.Add(Me.txb_ServerPassword, 3, 4)
        Me.TableLayoutPanel2.Controls.Add(Me.Label16, 2, 3)
        Me.TableLayoutPanel2.Controls.Add(Me.Label15, 2, 4)
        Me.TableLayoutPanel2.Controls.Add(Me.Label20, 0, 8)
        Me.TableLayoutPanel2.Controls.Add(Me.txb_ConnStr, 2, 8)
        Me.TableLayoutPanel2.Controls.Add(Me.btn_ConnStrBuild, 2, 6)
        Me.TableLayoutPanel2.Controls.Add(Me.btn_ConnTest, 2, 9)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(3, 3)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 12
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 66.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 12.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 18.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 51.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(720, 387)
        Me.TableLayoutPanel2.TabIndex = 2
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label17.Location = New System.Drawing.Point(3, 26)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(131, 26)
        Me.Label17.TabIndex = 6
        Me.Label17.Text = "База данных"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label19.Location = New System.Drawing.Point(3, 0)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(131, 26)
        Me.Label19.TabIndex = 1
        Me.Label19.Text = "Сервер"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txb_Server
        '
        Me.TableLayoutPanel2.SetColumnSpan(Me.txb_Server, 2)
        Me.txb_Server.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txb_Server.Location = New System.Drawing.Point(149, 3)
        Me.txb_Server.Name = "txb_Server"
        Me.txb_Server.Size = New System.Drawing.Size(221, 20)
        Me.txb_Server.TabIndex = 3
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label18.Location = New System.Drawing.Point(3, 52)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(131, 66)
        Me.Label18.TabIndex = 15
        Me.Label18.Text = "Аутентификация"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'GroupBox1
        '
        Me.TableLayoutPanel2.SetColumnSpan(Me.GroupBox1, 2)
        Me.GroupBox1.Controls.Add(Me.rdb_WindowsAuthentication)
        Me.GroupBox1.Controls.Add(rdb_SQLSrvAuth)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox1.Location = New System.Drawing.Point(149, 55)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(221, 60)
        Me.GroupBox1.TabIndex = 16
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Тип аутентификации"
        '
        'rdb_WindowsAuthentication
        '
        Me.rdb_WindowsAuthentication.AutoSize = True
        Me.rdb_WindowsAuthentication.Location = New System.Drawing.Point(6, 17)
        Me.rdb_WindowsAuthentication.Name = "rdb_WindowsAuthentication"
        Me.rdb_WindowsAuthentication.Size = New System.Drawing.Size(140, 17)
        Me.rdb_WindowsAuthentication.TabIndex = 13
        Me.rdb_WindowsAuthentication.Text = "Windows Authentication"
        Me.rdb_WindowsAuthentication.UseVisualStyleBackColor = True
        '
        'txb_DataBase
        '
        Me.TableLayoutPanel2.SetColumnSpan(Me.txb_DataBase, 2)
        Me.txb_DataBase.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txb_DataBase.Location = New System.Drawing.Point(149, 29)
        Me.txb_DataBase.Name = "txb_DataBase"
        Me.txb_DataBase.Size = New System.Drawing.Size(221, 20)
        Me.txb_DataBase.TabIndex = 18
        '
        'txb_ServerUser
        '
        Me.txb_ServerUser.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txb_ServerUser.Location = New System.Drawing.Point(210, 121)
        Me.txb_ServerUser.Name = "txb_ServerUser"
        Me.txb_ServerUser.Size = New System.Drawing.Size(160, 20)
        Me.txb_ServerUser.TabIndex = 9
        '
        'txb_ServerPassword
        '
        Me.txb_ServerPassword.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txb_ServerPassword.Location = New System.Drawing.Point(210, 147)
        Me.txb_ServerPassword.Name = "txb_ServerPassword"
        Me.txb_ServerPassword.Size = New System.Drawing.Size(160, 20)
        Me.txb_ServerPassword.TabIndex = 10
        Me.txb_ServerPassword.UseSystemPasswordChar = True
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label16.Location = New System.Drawing.Point(149, 118)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(55, 26)
        Me.Label16.TabIndex = 11
        Me.Label16.Text = "Имя"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label15.Location = New System.Drawing.Point(149, 144)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(55, 26)
        Me.Label15.TabIndex = 12
        Me.Label15.Text = "Пароль"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label20.Location = New System.Drawing.Point(3, 229)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(131, 51)
        Me.Label20.TabIndex = 19
        Me.Label20.Text = "Строка подключения"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'txb_ConnStr
        '
        Me.TableLayoutPanel2.SetColumnSpan(Me.txb_ConnStr, 3)
        Me.txb_ConnStr.Dock = System.Windows.Forms.DockStyle.Top
        Me.txb_ConnStr.Location = New System.Drawing.Point(149, 232)
        Me.txb_ConnStr.Multiline = True
        Me.txb_ConnStr.Name = "txb_ConnStr"
        Me.txb_ConnStr.Size = New System.Drawing.Size(568, 45)
        Me.txb_ConnStr.TabIndex = 20
        '
        'btn_ConnStrBuild
        '
        Me.TableLayoutPanel2.SetColumnSpan(Me.btn_ConnStrBuild, 2)
        Me.btn_ConnStrBuild.Dock = System.Windows.Forms.DockStyle.Top
        Me.btn_ConnStrBuild.Location = New System.Drawing.Point(149, 185)
        Me.btn_ConnStrBuild.Name = "btn_ConnStrBuild"
        Me.btn_ConnStrBuild.Size = New System.Drawing.Size(221, 23)
        Me.btn_ConnStrBuild.TabIndex = 21
        Me.btn_ConnStrBuild.Text = "Применить к строке подключения"
        Me.btn_ConnStrBuild.UseVisualStyleBackColor = True
        '
        'btn_ConnTest
        '
        Me.TableLayoutPanel2.SetColumnSpan(Me.btn_ConnTest, 2)
        Me.btn_ConnTest.Location = New System.Drawing.Point(149, 283)
        Me.btn_ConnTest.Name = "btn_ConnTest"
        Me.btn_ConnTest.Size = New System.Drawing.Size(221, 23)
        Me.btn_ConnTest.TabIndex = 22
        Me.btn_ConnTest.Text = "Проверить строку подключения"
        Me.btn_ConnTest.UseVisualStyleBackColor = True
        '
        'tbp_Project
        '
        Me.tbp_Project.Controls.Add(Me.TableLayoutPanel1)
        Me.tbp_Project.Location = New System.Drawing.Point(4, 22)
        Me.tbp_Project.Name = "tbp_Project"
        Me.tbp_Project.Padding = New System.Windows.Forms.Padding(3)
        Me.tbp_Project.Size = New System.Drawing.Size(726, 393)
        Me.tbp_Project.TabIndex = 0
        Me.tbp_Project.Text = "Проект"
        Me.tbp_Project.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 139.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 582.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.Label14, 0, 6)
        Me.TableLayoutPanel1.Controls.Add(Me.Label5, 0, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.lbl_WorkDir, 1, 6)
        Me.TableLayoutPanel1.Controls.Add(Me.Label4, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.txb_GUID, 1, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.Label3, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.Label2, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Label1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.txb_ProjectName, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.txb_ProjectDescpition, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.txb_CreatedAt, 1, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.txb_UpdatedAt, 1, 4)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(3, 3)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 7
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 73.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(720, 387)
        Me.TableLayoutPanel1.TabIndex = 1
        '
        'Label14
        '
        Me.Label14.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label14.Location = New System.Drawing.Point(3, 355)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(133, 32)
        Me.Label14.TabIndex = 16
        Me.Label14.Text = "Рабочий каталог"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label5.Location = New System.Drawing.Point(3, 151)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(133, 26)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = "Дата изменения"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lbl_WorkDir
        '
        Me.lbl_WorkDir.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_WorkDir.Location = New System.Drawing.Point(142, 355)
        Me.lbl_WorkDir.Name = "lbl_WorkDir"
        Me.lbl_WorkDir.Size = New System.Drawing.Size(576, 32)
        Me.lbl_WorkDir.TabIndex = 15
        Me.lbl_WorkDir.Text = "..."
        Me.lbl_WorkDir.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label4.Location = New System.Drawing.Point(3, 125)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(133, 26)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "Дата создания"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txb_GUID
        '
        Me.txb_GUID.Dock = System.Windows.Forms.DockStyle.Left
        Me.txb_GUID.Location = New System.Drawing.Point(142, 102)
        Me.txb_GUID.Name = "txb_GUID"
        Me.txb_GUID.Size = New System.Drawing.Size(220, 20)
        Me.txb_GUID.TabIndex = 7
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label3.Location = New System.Drawing.Point(3, 99)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(133, 26)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "GUID"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label2.Location = New System.Drawing.Point(3, 26)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(133, 73)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Описание"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label1.Location = New System.Drawing.Point(3, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(133, 26)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Название проекта"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txb_ProjectName
        '
        Me.txb_ProjectName.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txb_ProjectName.Location = New System.Drawing.Point(142, 3)
        Me.txb_ProjectName.Name = "txb_ProjectName"
        Me.txb_ProjectName.Size = New System.Drawing.Size(576, 20)
        Me.txb_ProjectName.TabIndex = 3
        '
        'txb_ProjectDescpition
        '
        Me.txb_ProjectDescpition.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txb_ProjectDescpition.Location = New System.Drawing.Point(142, 29)
        Me.txb_ProjectDescpition.Multiline = True
        Me.txb_ProjectDescpition.Name = "txb_ProjectDescpition"
        Me.txb_ProjectDescpition.Size = New System.Drawing.Size(576, 67)
        Me.txb_ProjectDescpition.TabIndex = 4
        '
        'txb_CreatedAt
        '
        Me.txb_CreatedAt.Dock = System.Windows.Forms.DockStyle.Left
        Me.txb_CreatedAt.Location = New System.Drawing.Point(142, 128)
        Me.txb_CreatedAt.Name = "txb_CreatedAt"
        Me.txb_CreatedAt.Size = New System.Drawing.Size(140, 20)
        Me.txb_CreatedAt.TabIndex = 9
        '
        'txb_UpdatedAt
        '
        Me.txb_UpdatedAt.Dock = System.Windows.Forms.DockStyle.Left
        Me.txb_UpdatedAt.Location = New System.Drawing.Point(142, 154)
        Me.txb_UpdatedAt.Name = "txb_UpdatedAt"
        Me.txb_UpdatedAt.Size = New System.Drawing.Size(140, 20)
        Me.txb_UpdatedAt.TabIndex = 10
        '
        'tbp_Tables
        '
        Me.tbp_Tables.Controls.Add(Me.SplitContainer1)
        Me.tbp_Tables.Controls.Add(Me.ckb_ShowExtraColumns)
        Me.tbp_Tables.Location = New System.Drawing.Point(4, 22)
        Me.tbp_Tables.Name = "tbp_Tables"
        Me.tbp_Tables.Padding = New System.Windows.Forms.Padding(3)
        Me.tbp_Tables.Size = New System.Drawing.Size(726, 393)
        Me.tbp_Tables.TabIndex = 1
        Me.tbp_Tables.Text = "Таблицы"
        Me.tbp_Tables.UseVisualStyleBackColor = True
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(3, 3)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.SplitContainer3)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.dgv_Columns)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label13)
        Me.SplitContainer1.Size = New System.Drawing.Size(720, 366)
        Me.SplitContainer1.SplitterDistance = 351
        Me.SplitContainer1.TabIndex = 0
        '
        'SplitContainer3
        '
        Me.SplitContainer3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer3.FixedPanel = System.Windows.Forms.FixedPanel.Panel2
        Me.SplitContainer3.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer3.Name = "SplitContainer3"
        Me.SplitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer3.Panel1
        '
        Me.SplitContainer3.Panel1.Controls.Add(Me.dgv_Tables)
        Me.SplitContainer3.Panel1.Controls.Add(Me.Label11)
        '
        'SplitContainer3.Panel2
        '
        Me.SplitContainer3.Panel2.Controls.Add(Me.txb_TableCondition)
        Me.SplitContainer3.Panel2.Controls.Add(Me.Label12)
        Me.SplitContainer3.Size = New System.Drawing.Size(351, 366)
        Me.SplitContainer3.SplitterDistance = 243
        Me.SplitContainer3.TabIndex = 0
        '
        'dgv_Tables
        '
        Me.dgv_Tables.AllowUserToAddRows = False
        Me.dgv_Tables.AllowUserToDeleteRows = False
        Me.dgv_Tables.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv_Tables.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgv_Tables.Location = New System.Drawing.Point(0, 13)
        Me.dgv_Tables.Name = "dgv_Tables"
        Me.dgv_Tables.RowHeadersVisible = False
        Me.dgv_Tables.Size = New System.Drawing.Size(351, 230)
        Me.dgv_Tables.TabIndex = 0
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label11.Location = New System.Drawing.Point(0, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(52, 13)
        Me.Label11.TabIndex = 1
        Me.Label11.Text = "Таблицы"
        '
        'txb_TableCondition
        '
        Me.txb_TableCondition.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txb_TableCondition.Location = New System.Drawing.Point(0, 13)
        Me.txb_TableCondition.Multiline = True
        Me.txb_TableCondition.Name = "txb_TableCondition"
        Me.txb_TableCondition.Size = New System.Drawing.Size(351, 106)
        Me.txb_TableCondition.TabIndex = 3
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label12.Location = New System.Drawing.Point(0, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(133, 13)
        Me.Label12.TabIndex = 2
        Me.Label12.Text = "Условие SQL на таблицу"
        '
        'dgv_Columns
        '
        Me.dgv_Columns.AllowUserToAddRows = False
        Me.dgv_Columns.AllowUserToDeleteRows = False
        Me.dgv_Columns.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv_Columns.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgv_Columns.Location = New System.Drawing.Point(0, 13)
        Me.dgv_Columns.Name = "dgv_Columns"
        Me.dgv_Columns.RowHeadersVisible = False
        Me.dgv_Columns.Size = New System.Drawing.Size(365, 353)
        Me.dgv_Columns.TabIndex = 0
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label13.Location = New System.Drawing.Point(0, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(50, 13)
        Me.Label13.TabIndex = 1
        Me.Label13.Text = "Колонки"
        '
        'ckb_ShowExtraColumns
        '
        Me.ckb_ShowExtraColumns.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.ckb_ShowExtraColumns.Location = New System.Drawing.Point(3, 369)
        Me.ckb_ShowExtraColumns.Name = "ckb_ShowExtraColumns"
        Me.ckb_ShowExtraColumns.Size = New System.Drawing.Size(720, 21)
        Me.ckb_ShowExtraColumns.TabIndex = 14
        Me.ckb_ShowExtraColumns.Text = "Дополнительные данные"
        Me.ckb_ShowExtraColumns.UseVisualStyleBackColor = True
        '
        'tbp_Dump
        '
        Me.tbp_Dump.Controls.Add(Me.TableLayoutPanel4)
        Me.tbp_Dump.Controls.Add(Me.ToolStrip3)
        Me.tbp_Dump.Location = New System.Drawing.Point(4, 22)
        Me.tbp_Dump.Name = "tbp_Dump"
        Me.tbp_Dump.Padding = New System.Windows.Forms.Padding(3)
        Me.tbp_Dump.Size = New System.Drawing.Size(726, 393)
        Me.tbp_Dump.TabIndex = 3
        Me.tbp_Dump.Text = "Выгрузка"
        Me.tbp_Dump.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel4
        '
        Me.TableLayoutPanel4.ColumnCount = 3
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 115.0!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 92.06612!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 48.0!))
        Me.TableLayoutPanel4.Controls.Add(Me.Label25, 0, 2)
        Me.TableLayoutPanel4.Controls.Add(Me.Label22, 0, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.txb_SQL, 1, 4)
        Me.TableLayoutPanel4.Controls.Add(Me.Label21, 0, 4)
        Me.TableLayoutPanel4.Controls.Add(Me.lbl_ProjectFile, 1, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.txb_DumpFile, 1, 2)
        Me.TableLayoutPanel4.Controls.Add(Me.btn_DumpFileChoose, 2, 2)
        Me.TableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel4.Location = New System.Drawing.Point(3, 28)
        Me.TableLayoutPanel4.Name = "TableLayoutPanel4"
        Me.TableLayoutPanel4.RowCount = 5
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 11.0!))
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 12.0!))
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.Size = New System.Drawing.Size(720, 362)
        Me.TableLayoutPanel4.TabIndex = 3
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label25.Location = New System.Drawing.Point(3, 31)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(109, 29)
        Me.Label25.TabIndex = 5
        Me.Label25.Text = "Файл выгрузки"
        Me.Label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label22.Location = New System.Drawing.Point(3, 0)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(109, 20)
        Me.Label22.TabIndex = 2
        Me.Label22.Text = "Файл проекта"
        Me.Label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txb_SQL
        '
        Me.TableLayoutPanel4.SetColumnSpan(Me.txb_SQL, 2)
        Me.txb_SQL.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txb_SQL.Location = New System.Drawing.Point(118, 75)
        Me.txb_SQL.Multiline = True
        Me.txb_SQL.Name = "txb_SQL"
        Me.txb_SQL.ReadOnly = True
        Me.txb_SQL.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txb_SQL.Size = New System.Drawing.Size(599, 284)
        Me.txb_SQL.TabIndex = 0
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label21.Location = New System.Drawing.Point(3, 72)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(109, 13)
        Me.Label21.TabIndex = 1
        Me.Label21.Text = "Запросы"
        Me.Label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lbl_ProjectFile
        '
        Me.lbl_ProjectFile.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_ProjectFile.Location = New System.Drawing.Point(118, 0)
        Me.lbl_ProjectFile.Name = "lbl_ProjectFile"
        Me.lbl_ProjectFile.Size = New System.Drawing.Size(551, 20)
        Me.lbl_ProjectFile.TabIndex = 6
        Me.lbl_ProjectFile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txb_DumpFile
        '
        Me.txb_DumpFile.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txb_DumpFile.Location = New System.Drawing.Point(118, 34)
        Me.txb_DumpFile.Name = "txb_DumpFile"
        Me.txb_DumpFile.Size = New System.Drawing.Size(551, 20)
        Me.txb_DumpFile.TabIndex = 7
        '
        'btn_DumpFileChoose
        '
        Me.btn_DumpFileChoose.Location = New System.Drawing.Point(675, 34)
        Me.btn_DumpFileChoose.Name = "btn_DumpFileChoose"
        Me.btn_DumpFileChoose.Size = New System.Drawing.Size(42, 23)
        Me.btn_DumpFileChoose.TabIndex = 8
        Me.btn_DumpFileChoose.Text = "..."
        Me.btn_DumpFileChoose.UseVisualStyleBackColor = True
        '
        'ToolStrip3
        '
        Me.ToolStrip3.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1, Me.ToolStripSeparator1, Me.tsb_Dump})
        Me.ToolStrip3.Location = New System.Drawing.Point(3, 3)
        Me.ToolStrip3.Name = "ToolStrip3"
        Me.ToolStrip3.Size = New System.Drawing.Size(720, 25)
        Me.ToolStrip3.TabIndex = 2
        Me.ToolStrip3.Text = "ToolStrip3"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(120, 22)
        Me.ToolStripButton1.Text = "Показать запросы"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'tsb_Dump
        '
        Me.tsb_Dump.Image = CType(resources.GetObject("tsb_Dump.Image"), System.Drawing.Image)
        Me.tsb_Dump.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsb_Dump.Name = "tsb_Dump"
        Me.tsb_Dump.Size = New System.Drawing.Size(123, 22)
        Me.tsb_Dump.Text = "Выгрузить данные"
        '
        'ImageList_Dump
        '
        Me.ImageList_Dump.ImageStream = CType(resources.GetObject("ImageList_Dump.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList_Dump.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList_Dump.Images.SetKeyName(0, "dump_db_2")
        Me.ImageList_Dump.Images.SetKeyName(1, "dump_db")
        Me.ImageList_Dump.Images.SetKeyName(2, "SQL")
        Me.ImageList_Dump.Images.SetKeyName(3, "dump_tunes")
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.SplitContainer2)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(551, 301)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Проект"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'SplitContainer2
        '
        Me.SplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer2.Location = New System.Drawing.Point(3, 3)
        Me.SplitContainer2.Name = "SplitContainer2"
        '
        'SplitContainer2.Panel1
        '
        Me.SplitContainer2.Panel1.Controls.Add(Me.TableLayoutPanel5)
        '
        'SplitContainer2.Panel2
        '
        Me.SplitContainer2.Panel2.Controls.Add(Me.TableLayoutPanel6)
        Me.SplitContainer2.Size = New System.Drawing.Size(545, 295)
        Me.SplitContainer2.SplitterDistance = 181
        Me.SplitContainer2.TabIndex = 1
        '
        'TableLayoutPanel5
        '
        Me.TableLayoutPanel5.ColumnCount = 1
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel5.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel5.Name = "TableLayoutPanel5"
        Me.TableLayoutPanel5.RowCount = 1
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel5.Size = New System.Drawing.Size(181, 295)
        Me.TableLayoutPanel5.TabIndex = 1
        '
        'TableLayoutPanel6
        '
        Me.TableLayoutPanel6.ColumnCount = 2
        Me.TableLayoutPanel6.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 139.0!))
        Me.TableLayoutPanel6.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 358.0!))
        Me.TableLayoutPanel6.Controls.Add(Me.Label6, 0, 4)
        Me.TableLayoutPanel6.Controls.Add(Me.Label7, 0, 3)
        Me.TableLayoutPanel6.Controls.Add(Me.TextBox1, 1, 2)
        Me.TableLayoutPanel6.Controls.Add(Me.Label8, 0, 2)
        Me.TableLayoutPanel6.Controls.Add(Me.Label9, 0, 1)
        Me.TableLayoutPanel6.Controls.Add(Me.Label10, 0, 0)
        Me.TableLayoutPanel6.Controls.Add(Me.TextBox2, 1, 0)
        Me.TableLayoutPanel6.Controls.Add(Me.TextBox3, 1, 1)
        Me.TableLayoutPanel6.Controls.Add(Me.TextBox4, 1, 3)
        Me.TableLayoutPanel6.Controls.Add(Me.TextBox5, 1, 4)
        Me.TableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel6.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel6.Name = "TableLayoutPanel6"
        Me.TableLayoutPanel6.RowCount = 7
        Me.TableLayoutPanel6.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel6.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel6.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel6.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel6.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel6.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 11.0!))
        Me.TableLayoutPanel6.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 95.0!))
        Me.TableLayoutPanel6.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 68.72727!))
        Me.TableLayoutPanel6.Size = New System.Drawing.Size(360, 295)
        Me.TableLayoutPanel6.TabIndex = 1
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label6.Location = New System.Drawing.Point(3, 140)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(133, 26)
        Me.Label6.TabIndex = 12
        Me.Label6.Text = "Дата редактирования"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label7.Location = New System.Drawing.Point(3, 114)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(133, 26)
        Me.Label7.TabIndex = 11
        Me.Label7.Text = "Дата создания"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TextBox1
        '
        Me.TextBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TextBox1.Location = New System.Drawing.Point(142, 91)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(352, 20)
        Me.TextBox1.TabIndex = 7
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label8.Location = New System.Drawing.Point(3, 88)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(133, 26)
        Me.Label8.TabIndex = 6
        Me.Label8.Text = "GUID"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label9.Location = New System.Drawing.Point(3, 26)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(133, 62)
        Me.Label9.TabIndex = 5
        Me.Label9.Text = "Описание"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label10.Location = New System.Drawing.Point(3, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(133, 26)
        Me.Label10.TabIndex = 1
        Me.Label10.Text = "Название проекта"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TextBox2
        '
        Me.TextBox2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TextBox2.Location = New System.Drawing.Point(142, 3)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(352, 20)
        Me.TextBox2.TabIndex = 3
        '
        'TextBox3
        '
        Me.TextBox3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TextBox3.Location = New System.Drawing.Point(142, 29)
        Me.TextBox3.Multiline = True
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(352, 56)
        Me.TextBox3.TabIndex = 4
        '
        'TextBox4
        '
        Me.TextBox4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TextBox4.Location = New System.Drawing.Point(142, 117)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(352, 20)
        Me.TextBox4.TabIndex = 9
        '
        'TextBox5
        '
        Me.TextBox5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TextBox5.Location = New System.Drawing.Point(142, 143)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New System.Drawing.Size(352, 20)
        Me.TextBox5.TabIndex = 10
        '
        'TabPage4
        '
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage4.Size = New System.Drawing.Size(751, 352)
        Me.TabPage4.TabIndex = 1
        Me.TabPage4.Text = "TabPage2"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'TabPage5
        '
        Me.TabPage5.Controls.Add(Me.TableLayoutPanel7)
        Me.TabPage5.Location = New System.Drawing.Point(4, 22)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage5.Size = New System.Drawing.Size(751, 352)
        Me.TabPage5.TabIndex = 2
        Me.TabPage5.Text = "TabPage3"
        Me.TabPage5.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel7
        '
        Me.TableLayoutPanel7.ColumnCount = 1
        Me.TableLayoutPanel7.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel7.Controls.Add(Me.DataGridView1, 0, 0)
        Me.TableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel7.Location = New System.Drawing.Point(3, 3)
        Me.TableLayoutPanel7.Name = "TableLayoutPanel7"
        Me.TableLayoutPanel7.RowCount = 2
        Me.TableLayoutPanel7.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 70.0!))
        Me.TableLayoutPanel7.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel7.Size = New System.Drawing.Size(745, 346)
        Me.TableLayoutPanel7.TabIndex = 10
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(3, 3)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(739, 64)
        Me.DataGridView1.TabIndex = 8
        '
        'newToolStripButton
        '
        Me.newToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.newToolStripButton.Image = CType(resources.GetObject("newToolStripButton.Image"), System.Drawing.Image)
        Me.newToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.newToolStripButton.Name = "newToolStripButton"
        Me.newToolStripButton.Size = New System.Drawing.Size(23, 22)
        Me.newToolStripButton.Text = "&New"
        '
        'openToolStripButton
        '
        Me.openToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.openToolStripButton.Image = CType(resources.GetObject("openToolStripButton.Image"), System.Drawing.Image)
        Me.openToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.openToolStripButton.Name = "openToolStripButton"
        Me.openToolStripButton.Size = New System.Drawing.Size(23, 22)
        Me.openToolStripButton.Text = "&Open"
        '
        'saveToolStripButton
        '
        Me.saveToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.saveToolStripButton.Image = CType(resources.GetObject("saveToolStripButton.Image"), System.Drawing.Image)
        Me.saveToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.saveToolStripButton.Name = "saveToolStripButton"
        Me.saveToolStripButton.Size = New System.Drawing.Size(23, 22)
        Me.saveToolStripButton.Text = "&Save"
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsb_New, Me.tsb_Open, Me.tsb_Save})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(734, 25)
        Me.ToolStrip1.TabIndex = 18
        Me.ToolStrip1.Text = "toolStrip1"
        '
        'tsb_New
        '
        Me.tsb_New.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsb_New.Image = CType(resources.GetObject("tsb_New.Image"), System.Drawing.Image)
        Me.tsb_New.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsb_New.Name = "tsb_New"
        Me.tsb_New.Size = New System.Drawing.Size(23, 22)
        Me.tsb_New.Text = "&New"
        '
        'tsb_Open
        '
        Me.tsb_Open.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsb_Open.Image = CType(resources.GetObject("tsb_Open.Image"), System.Drawing.Image)
        Me.tsb_Open.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsb_Open.Name = "tsb_Open"
        Me.tsb_Open.Size = New System.Drawing.Size(23, 22)
        Me.tsb_Open.Text = "&Open"
        '
        'tsb_Save
        '
        Me.tsb_Save.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsb_Save.Image = CType(resources.GetObject("tsb_Save.Image"), System.Drawing.Image)
        Me.tsb_Save.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsb_Save.Name = "tsb_Save"
        Me.tsb_Save.Size = New System.Drawing.Size(23, 22)
        Me.tsb_Save.Text = "&Save"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(734, 444)
        Me.Controls.Add(Me.TabControl_Main)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Form1"
        Me.Text = "Настройщик выгрузки"
        Me.TabControl_Main.ResumeLayout(False)
        Me.tbp_DB.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.tbp_Project.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.tbp_Tables.ResumeLayout(False)
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.Panel2.PerformLayout()
        Me.SplitContainer1.ResumeLayout(False)
        Me.SplitContainer3.Panel1.ResumeLayout(False)
        Me.SplitContainer3.Panel1.PerformLayout()
        Me.SplitContainer3.Panel2.ResumeLayout(False)
        Me.SplitContainer3.Panel2.PerformLayout()
        Me.SplitContainer3.ResumeLayout(False)
        CType(Me.dgv_Tables, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgv_Columns, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbp_Dump.ResumeLayout(False)
        Me.tbp_Dump.PerformLayout()
        Me.TableLayoutPanel4.ResumeLayout(False)
        Me.TableLayoutPanel4.PerformLayout()
        Me.ToolStrip3.ResumeLayout(False)
        Me.ToolStrip3.PerformLayout()
        Me.TabPage1.ResumeLayout(False)
        Me.SplitContainer2.Panel1.ResumeLayout(False)
        Me.SplitContainer2.Panel2.ResumeLayout(False)
        Me.SplitContainer2.ResumeLayout(False)
        Me.TableLayoutPanel6.ResumeLayout(False)
        Me.TableLayoutPanel6.PerformLayout()
        Me.TabPage5.ResumeLayout(False)
        Me.TableLayoutPanel7.ResumeLayout(False)
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TabControl_Main As System.Windows.Forms.TabControl
    Friend WithEvents tbp_Project As System.Windows.Forms.TabPage
    Friend WithEvents tbp_Tables As System.Windows.Forms.TabPage
    Friend WithEvents dgv_Tables As System.Windows.Forms.DataGridView
    Friend WithEvents dgv_Columns As System.Windows.Forms.DataGridView
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txb_GUID As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txb_ProjectName As System.Windows.Forms.TextBox
    Friend WithEvents txb_ProjectDescpition As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txb_CreatedAt As System.Windows.Forms.TextBox
    Friend WithEvents txb_UpdatedAt As System.Windows.Forms.TextBox
    Friend WithEvents ckb_ShowExtraColumns As System.Windows.Forms.CheckBox
    Friend WithEvents lbl_WorkDir As System.Windows.Forms.Label
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents SplitContainer2 As System.Windows.Forms.SplitContainer
    Friend WithEvents TableLayoutPanel5 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents TableLayoutPanel6 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage5 As System.Windows.Forms.TabPage
    Friend WithEvents TableLayoutPanel7 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txb_TableCondition As System.Windows.Forms.TextBox
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents SplitContainer3 As System.Windows.Forms.SplitContainer
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents ImageList_Dump As System.Windows.Forms.ImageList
    Friend WithEvents tbp_DB As System.Windows.Forms.TabPage
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents txb_Server As System.Windows.Forms.TextBox
    Friend WithEvents txb_ServerUser As System.Windows.Forms.TextBox
    Friend WithEvents txb_ServerPassword As System.Windows.Forms.TextBox
    Friend WithEvents rdb_WindowsAuthentication As System.Windows.Forms.RadioButton
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txb_DataBase As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents txb_ConnStr As System.Windows.Forms.TextBox
    Friend WithEvents btn_ConnStrBuild As System.Windows.Forms.Button
    Friend WithEvents btn_ConnTest As System.Windows.Forms.Button
    Friend WithEvents newToolStripButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents openToolStripButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents saveToolStripButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents tsb_New As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsb_Open As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsb_Save As System.Windows.Forms.ToolStripButton
    Friend WithEvents tbp_Dump As System.Windows.Forms.TabPage
    Friend WithEvents txb_SQL As System.Windows.Forms.TextBox
    Friend WithEvents ToolStrip3 As System.Windows.Forms.ToolStrip
    Friend WithEvents tsb_Dump As System.Windows.Forms.ToolStripButton
    Friend WithEvents TableLayoutPanel4 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents lbl_ProjectFile As System.Windows.Forms.Label
    Friend WithEvents txb_DumpFile As System.Windows.Forms.TextBox
    Friend WithEvents btn_DumpFileChoose As System.Windows.Forms.Button

End Class
