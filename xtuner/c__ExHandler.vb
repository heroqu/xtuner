﻿Public Class c__ExHandler

    Delegate Sub HandlerDelegate(ByVal ex_ As Exception)

    Private _handler As HandlerDelegate

    Public Sub New(Optional ByVal handler_ As c__ExHandler.HandlerDelegate = Nothing)
        _handler = handler_
    End Sub

    Public Sub Handle(ByVal ex As Exception)
        If Not _handler Is Nothing Then
            _handler.Invoke(ex)
        End If
    End Sub

End Class