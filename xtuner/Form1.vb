Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Text


Public Class Form1

    Private _P As c__XProject

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        _start_new_project()
        _set_form_caption()
    End Sub

    Private Sub _start_new_project()
        _P = New c__XProject(Conn_str_default)
        _BindControls()
        _TuneColumns()
    End Sub

    Private Sub _BindControls()

        ' Grid Databinding

        dgv_Tables.DataSource = _P.ds_ViewManager
        dgv_Tables.DataMember = "XTables"

        dgv_Columns.DataSource = _P.ds_ViewManager
        dgv_Columns.DataMember = "XTables.relXTableXColumn"

        txb_TableCondition.DataBindings.Clear()
        txb_TableCondition.DataBindings.Add("Text", _P.ds_ViewManager, "XTables.Condition")

        ' Header Databindings
        txb_ProjectName.DataBindings.Clear()
        txb_ProjectName.DataBindings.Add("Text", _P.ds_ViewManager, "XMeta.Name")

        txb_ProjectDescpition.DataBindings.Clear()
        txb_ProjectDescpition.DataBindings.Add("Text", _P.ds_ViewManager, "XMeta.Description")

        txb_GUID.DataBindings.Clear()
        txb_GUID.DataBindings.Add("Text", _P.ds_ViewManager, "XMeta.GUID")

        txb_CreatedAt.DataBindings.Clear()
        txb_CreatedAt.DataBindings.Add("Text", _P.ds_ViewManager, "XMeta.CreatedAt")

        txb_UpdatedAt.DataBindings.Clear()
        txb_UpdatedAt.DataBindings.Add("Text", _P.ds_ViewManager, "XMeta.UpdatedAt")

        ' Application params bindings
        ckb_ShowExtraColumns.DataBindings.Clear()
        ckb_ShowExtraColumns.DataBindings.Add("Checked", App_params.ds_ViewManager, "Data.ShowExtraColumns")

        lbl_WorkDir.DataBindings.Clear()
        lbl_WorkDir.DataBindings.Add("Text", App_params.ds_ViewManager, "Data.WorkDir")


        ' Connection string params

        txb_Server.DataBindings.Clear()
        txb_Server.DataBindings.Add("Text", App_params.ds_ViewManager, "Data.Server")

        txb_DataBase.DataBindings.Clear()
        txb_DataBase.DataBindings.Add("Text", App_params.ds_ViewManager, "Data.DataBase")


        rdb_WindowsAuthentication.Checked = False

        rdb_WindowsAuthentication.DataBindings.Clear()
        rdb_WindowsAuthentication.DataBindings.Add("Checked", App_params.ds_ViewManager, "Data.WindowsAuthentication")


        txb_ServerUser.DataBindings.Clear()
        txb_ServerUser.DataBindings.Add("Text", App_params.ds_ViewManager, "Data.ServerUser")

        txb_ServerPassword.DataBindings.Clear()
        txb_ServerPassword.DataBindings.Add("Text", App_params.ds_ViewManager, "Data.ServerPassword")

        txb_ConnStr.DataBindings.Clear()
        txb_ConnStr.DataBindings.Add("Text", App_params.ds_ViewManager, "Data.ConnectionString")


        '' Dump params

        'lbl_ProjectFile.DataBindings.Clear()
        'lbl_ProjectFile.DataBindings.Add("Text", App_params.ds_ViewManager, "Data.WorkDir")


    End Sub

    Private Function _BuildConnectionString() As String
        'Data Source=1-ПК;Initial Catalog=Northwind;Integrated Security=True
        'Data Source=1-ПК;Persist Security Info=True;User ID=sa
        '"Data Source=localhost;Persist Security Info=True;User ID=sa;Password=sa;Unicode=True"

        Dim sb_ As New StringBuilder

        Dim tmp_ As String

        ' Data Source
        tmp_ = App_params.Value(PARAM_CONN_SRV)
        If String.IsNullOrEmpty(tmp_) Then
            Throw New ArgumentNullException(PARAM_CONN_SRV, "Не задан параметр соединения!")
        End If
        sb_.Append("Data Source=")
        sb_.Append(tmp_)

        ' Initial Catalog
        tmp_ = App_params.Value(PARAM_CONN_DB)
        If String.IsNullOrEmpty(tmp_) Then
            Throw New ArgumentNullException(PARAM_CONN_DB, "Не задан параметр соединения!")
        End If
        sb_.Append(";Initial Catalog=")
        sb_.Append(tmp_)

        ' Security
        If App_params.Value(PARAM_CONN_AUTH_WIN) = True Then
            sb_.Append(";Integrated Security=True")
        Else
            sb_.Append(";Persist Security Info=True")

            tmp_ = App_params.Value(PARAM_CONN_USER)
            If String.IsNullOrEmpty(tmp_) Then
                Throw New ArgumentNullException(PARAM_CONN_USER, "Не задан параметр соединения!")
            End If
            sb_.Append(";User ID=")
            sb_.Append(tmp_)

            tmp_ = App_params.Value(PARAM_CONN_PASS)
            If Not String.IsNullOrEmpty(tmp_) Then
                sb_.Append(";Password=")
                sb_.Append(tmp_)
            End If
        End If

        Return sb_.ToString

    End Function

    Private Sub _TuneColumns()

        Dim _show_extra_columns As Boolean
        '_show_extra_columns = App_params.Value(PARAM_SHOW_EXTRA_COLUMNS)

        _show_extra_columns = Me.ckb_ShowExtraColumns.Checked

        With dgv_Tables
            .Columns("Yes").Resizable = DataGridViewTriState.False
            .Columns("Yes").AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader

            .Columns("TableName").ReadOnly = True
            .Columns("TableName").HeaderText = "Таблица"
            .Columns("TableName").Resizable = DataGridViewTriState.True
            .Columns("TableName").Width = 150

            .Columns("Condition").ReadOnly = False
            .Columns("Condition").HeaderText = "Условие SQL"
            .Columns("Condition").Resizable = DataGridViewTriState.True
            .Columns("Condition").Width = 100
            .Columns("TableID").AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader


            .Columns("TableID").Visible = _show_extra_columns
            .Columns("TableID").ReadOnly = True
            .Columns("TableID").HeaderText = "Table ID"
            .Columns("TableID").Resizable = DataGridViewTriState.False
            .Columns("TableID").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            '.Columns("TableID").Width = 33
        End With

        With dgv_Columns
            .Columns("Yes").Resizable = DataGridViewTriState.False
            .Columns("Yes").AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader

            .Columns("ColumnID").ReadOnly = True
            .Columns("ColumnID").HeaderText = "ID"
            .Columns("ColumnID").Resizable = DataGridViewTriState.False
            .Columns("ColumnID").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            .Columns("ColumnID").Width = 28

            .Columns("ColumnName").HeaderText = "Колонка"
            .Columns("ColumnName").Resizable = DataGridViewTriState.True
            .Columns("ColumnName").Width = 150

            .Columns("ColumnCaption").HeaderText = "Заголовок"
            .Columns("ColumnCaption").Resizable = DataGridViewTriState.True
            .Columns("ColumnCaption").Width = 110

            .Columns("TypeName").HeaderText = "Тип"
            .Columns("TypeName").Resizable = DataGridViewTriState.False
            .Columns("TypeName").AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader
            .Columns("TypeName").ReadOnly = True
            .Columns("TypeName").Visible = _show_extra_columns

            .Columns("MaxLength").HeaderText = "Макс. Длина"
            .Columns("MaxLength").AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader
            .Columns("MaxLength").Resizable = DataGridViewTriState.False
            .Columns("MaxLength").ReadOnly = True
            .Columns("MaxLength").Visible = _show_extra_columns

            .Columns("IsNullable").HeaderText = "Nullable"
            .Columns("IsNullable").AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader
            .Columns("IsNullable").Resizable = DataGridViewTriState.False
            .Columns("IsNullable").ReadOnly = True
            .Columns("IsNullable").Visible = _show_extra_columns

            .Columns("IsNullable").AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader

            .Columns("TableID").ReadOnly = True
            .Columns("TableID").Visible = _show_extra_columns
            .Columns("TableID").HeaderText = "Table ID"
            .Columns("TableID").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
        End With

    End Sub

    Private Sub ckb_ShowExtraColumnsCheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ckb_ShowExtraColumns.CheckedChanged
        App_configer.SetValue(PARAM_SHOW_EXTRA_COLUMNS, ckb_ShowExtraColumns.Checked)
        _TuneColumns()
    End Sub

    Private Sub _save_project(Optional ByVal _overwrite_prompt As Boolean = True)
        Try
            Dim sfd_ As New SaveFileDialog()
            sfd_.Filter = "Файлы проектов|*.xprj"
            sfd_.Title = "Сохранить проект..."
            sfd_.RestoreDirectory = True
            sfd_.OverwritePrompt = _overwrite_prompt

            If String.IsNullOrEmpty(CurrentFileName) Then
                sfd_.FileName = _P.Name
            Else
                sfd_.FileName = Path.GetFileName(CurrentFileName)
            End If

            sfd_.ShowDialog()

            If sfd_.FileName <> "" Then
                Dim fs_ As System.IO.FileStream = CType _
                   (sfd_.OpenFile(), System.IO.FileStream)

                _P.WriteXml(fs_)

                fs_.Close()

                _set_current_file(sfd_.FileName)

            End If
        Catch ex_ As Exception
            App_ex_handler.Handle(ex_)
        End Try
    End Sub

    Private Sub _set_current_file(ByVal file_name_ As String)
        CurrentFileName = file_name_
        App_params.SetValue(PARAM_WORK_DIR, Path.GetDirectoryName(file_name_))
        lbl_ProjectFile.Text = file_name_

        _set_form_caption()
    End Sub
    Private Sub _set_form_caption()
        Dim sb_ As New StringBuilder
        sb_.Append(My.Application.Info.Title)
        If Not String.IsNullOrEmpty(CurrentFileName) Then
            sb_.Append("   - ")
            sb_.Append(CurrentFileName)
        End If
        Me.Text = sb_.ToString
    End Sub

    Private Sub _open_project()
        Try
            Dim stream_ As Stream
            Dim ofd_ As New OpenFileDialog()

            ofd_.Title = "Открыть проект из файла..."
            ofd_.Filter = "Файлы проектов (*.xprj)|*.xprj|Все файлы (*.*)|*.*"
            ofd_.FilterIndex = 1
            ofd_.RestoreDirectory = True

            If ofd_.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
                stream_ = ofd_.OpenFile()
                If Not (stream_ Is Nothing) Then

                    _P.ReadXml(stream_)

                    stream_.Close()

                    _set_current_file(ofd_.FileName)

                    _BindControls()
                    _TuneColumns()
                End If
            End If
        Catch ex_ As Exception
            App_ex_handler.Handle(ex_)
        End Try
    End Sub



    Private Sub _end_edit_project()
        Dim list_ As New List(Of Control)
        With list_
            .Add(dgv_Tables)
            .Add(dgv_Columns)
            .Add(txb_ProjectName)
            .Add(txb_ProjectDescpition)
            .Add(txb_CreatedAt)
            .Add(txb_UpdatedAt)
            .Add(txb_TableCondition)
        End With
        _force_end_edit(list_)
    End Sub

    Private Sub _force_end_edit(ByVal ctls_ As List(Of Control))
        For Each ctl_ As Control In ctls_
            ctl_.Enabled = Not ctl_.Enabled
            ctl_.Enabled = Not ctl_.Enabled
        Next
    End Sub



    Private Sub Label18_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub btn_ConnStrBuild_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_ConnStrBuild.Click
        Try
            Dim c_str_ As String = _BuildConnectionString()
            'txb_ConnStr.Text = c_str_
            App_params.SetValue(PARAM_CONN_STR, c_str_)
            'txb_ConnStr.Enabled = False
            'txb_ConnStr.Enabled = True
            'Dim qq = App_params.Value(PARAM_CONN_STR)
            'Dim qqq = qq

            txb_ConnStr.Text = App_params.Value(PARAM_CONN_STR)

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub



    Private Sub rdb_WindowsAuthentication_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdb_WindowsAuthentication.CheckedChanged
        App_params.SetValue(PARAM_CONN_AUTH_WIN, rdb_WindowsAuthentication.Checked)
    End Sub

    Private Sub txb_DataBase_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txb_DataBase.TextChanged
        App_params.SetValue(PARAM_CONN_DB, txb_DataBase.Text)
    End Sub

    Private Sub txb_Server_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txb_Server.TextChanged
        App_params.SetValue(PARAM_CONN_SRV, txb_Server.Text)
    End Sub

    Private Sub txb_ServerUser_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txb_ServerUser.TextChanged
        App_params.SetValue(PARAM_CONN_USER, txb_ServerUser.Text)
    End Sub

    Private Sub txb_ServerPassword_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txb_ServerPassword.TextChanged
        App_params.SetValue(PARAM_CONN_PASS, txb_ServerPassword.Text)
    End Sub


    Private Sub btn_ConnTest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_ConnTest.Click
        Try
            If TestConnection(App_params.Value(PARAM_CONN_STR)) = True Then
                MsgBox("Проверка соединения прошла успешно!", MsgBoxStyle.Information)
            Else
                MsgBox("Проверка не прошла!", MsgBoxStyle.Critical)
            End If
        Catch ex As Exception
            MsgBox("Проверка не прошла!" _
                & Microsoft.VisualBasic.ControlChars.NewLine & _
                ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub


    Private Sub tsb_New_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsb_New.Click
        _start_new_project()
    End Sub

    Private Sub tsb_Open_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsb_Open.Click
        _open_project()
    End Sub

    Private Sub tsb_Save_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsb_Save.Click
        _end_edit_project()
        _save_project()
    End Sub

    Private Sub tsb_Dump_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub



    Private Sub _get_dump_params()
        Dim queries_ As List(Of String) = _P.GetAllQueries()
        Dim sb_ As New StringBuilder
        For Each q_ As String In queries_
            sb_.Append(q_)
            sb_.AppendLine("---------")
        Next
        txb_SQL.Text = sb_.ToString
    End Sub


    Private Sub tsb_Dump_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsb_Dump.Click
        Dim sb_ As New StringBuilder
        Try
            c__XProject.Dump(ConnStr, lbl_ProjectFile.Text, txb_DumpFile.Text)
            sb_.AppendLine("Данные предположительно выгружены в файл:")
            sb_.Append(ControlChars.Tab)
            sb_.AppendLine(txb_DumpFile.Text)

            MsgBox(sb_.ToString, MsgBoxStyle.Information)
        Catch ex As Exception
            sb_.AppendLine("Не удалось выгрузить данные:")
            sb_.Append(ControlChars.Tab)
            sb_.AppendLine(ex.Message)

            MsgBox(sb_.ToString, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        _get_dump_params()
    End Sub

    Private Sub btn_DumpFileChoose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_DumpFileChoose.Click
        Try
            Dim sfd_ As New SaveFileDialog()
            sfd_.Filter = "Файлы выгрузки|*.xml"
            sfd_.Title = "Выбрать имя файла для выгрузки..."
            sfd_.RestoreDirectory = True
            sfd_.OverwritePrompt = True

            If Not _P Is Nothing Then
                sfd_.FileName = _P.Name
            End If

            If Not sfd_.ShowDialog() = Windows.Forms.DialogResult.Cancel _
                    AndAlso sfd_.FileName <> "" _
                    Then

                txb_DumpFile.Text = sfd_.FileName

            End If
        Catch ex_ As Exception
            App_ex_handler.Handle(ex_)
        End Try
    End Sub
End Class