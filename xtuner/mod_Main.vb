﻿Imports System
Imports System.Data
Imports System.Data.SqlClient


Module mod_Main

    Public CurrentFileName As String = Nothing
    Public App_configer As c__Configer
    Public App_ex_handler As c__ExHandler
    Public App_params As c__Params
    Private _main_form As Form1

    Public Conn_str_default As String = _
        "Data Source=1-ПК;Initial Catalog=Northwind;Integrated Security=True"

    Const REGISTRY_SUBKEY As String = "XProject"

    Public Const PARAM_SHOW_EXTRA_COLUMNS As String = "ShowExtraColumns"
    Public Const PARAM_WORK_DIR As String = "WorkDir"

    Public Const PARAM_CONN_SRV As String = "Server"
    Public Const PARAM_CONN_DB As String = "DataBase"
    Public Const PARAM_CONN_AUTH_WIN As String = "WindowsAuthentication"
    Public Const PARAM_CONN_USER As String = "ServerUser"
    Public Const PARAM_CONN_PASS As String = "ServerPassword"
    Public Const PARAM_CONN_STR As String = "ConnectionString"


    Public Sub Main()
        App_ex_handler = New c__ExHandler(AddressOf _application_ex_handler)

        Try
            App_configer = New c__Configer(REGISTRY_SUBKEY)

            App_params = New c__Params(App_configer)
            App_params.AddParam(PARAM_SHOW_EXTRA_COLUMNS, GetType(Boolean))
            App_params.AddParam(PARAM_WORK_DIR, GetType(String))

            App_params.AddParam(PARAM_CONN_SRV, GetType(String))
            App_params.AddParam(PARAM_CONN_DB, GetType(String))
            App_params.AddParam(PARAM_CONN_AUTH_WIN, GetType(Boolean))
            App_params.AddParam(PARAM_CONN_USER, GetType(String))
            App_params.AddParam(PARAM_CONN_PASS, GetType(String))
            App_params.AddParam(PARAM_CONN_STR, GetType(String))

            App_params.Load()

            _main_form = New Form1
            Application.Run(_main_form)

            App_params.Save()

        Catch ex_ As Exception
            App_ex_handler.Handle(ex_)
        End Try
    End Sub

    Private Sub _application_ex_handler(ByVal ex_ As Exception)
        ' just a stub for now
        Console.WriteLine(ex_.Message)
        Console.ReadLine()
        MsgBox(ex_.Message, MsgBoxStyle.Exclamation)
        'Throw ex_
    End Sub

    Public Function ConnStr() As String
        Return App_params.Value(PARAM_CONN_STR)
    End Function

    Public Function TestConnection(ByVal conn_str_ As String) As Boolean

        Dim conn_ As New SqlConnection(conn_str_)

        Try
            conn_.Open()

            ' Fill the Dataset with XTables, map Default Tablename
            ' "Table" to "XTables".
            Dim query_ As String = _
                 "SELECT                 " & _
                 "	COUNT(*)             " & _
                 "FROM                   " & _
                 "	sys.objects          "

            Dim cmd_ As New SqlCommand(query_, conn_)

            Dim dr = cmd_.ExecuteReader()

            Return True

        Catch ex_ As Exception
            App_ex_handler.Handle(ex_)
        Finally
            conn_.Close()
        End Try

        Return False

    End Function

End Module



