﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Security.Permissions
Imports Microsoft.Win32

<Assembly: RegistryPermissionAttribute( _
    SecurityAction.RequestMinimum, ViewAndModify:="HKEY_CURRENT_USER")> 
Public Class c__Configer
    Implements i__DataStore

    Private _RegistryKey As RegistryKey

    Private _ex_handler As c__ExHandler

    ' default exception handling method
    Private Sub _default_ex_handling_sub(ByVal ex_ As Exception)
        ' just a stub for now
        Throw ex_
    End Sub

    Public Sub New( _
            ByVal registry_subkey_name_ As String, _
            Optional ByVal ex_handler_ As c__ExHandler = Nothing _
            )

        If ex_handler_ Is Nothing Then
            ' default handler
            _ex_handler = New c__ExHandler(AddressOf _default_ex_handling_sub)
        Else
            _ex_handler = ex_handler_
        End If

        Dim _key As RegistryKey = Registry.CurrentUser.CreateSubKey("Software")

        _RegistryKey = _key.CreateSubKey(registry_subkey_name_, RegistryKeyPermissionCheck.ReadWriteSubTree)

    End Sub

    Public Sub SetValue(ByVal key_ As String, ByVal value_ As Object) Implements i__DataStore.SetValue
        Try
            _RegistryKey.SetValue(key_, value_)
        Catch ex As Exception
            _ex_handler.Handle(ex)
        End Try
    End Sub

    Public Function Value(ByVal key_ As String) As Object Implements i__DataStore.Value
        Dim result_ As Object = Nothing
        Try
            result_ = _RegistryKey.GetValue(key_)
        Catch ex As Exception
            _ex_handler.Handle(ex)
        End Try
        Return result_
    End Function

    Public Sub Load() Implements i__DataStore.Load

    End Sub

    Public Sub Save() Implements i__DataStore.Save

    End Sub
End Class

