﻿Imports System.Data


Public Class c__Params
    Implements i__DataStore

    Private _dt_Data As DataTable
    Private _ds_Main As DataSet
    Private _ds_ViewManager As DataViewManager

    Private _store As i__DataStore

    Public Sub New(ByVal store_ As i__DataStore)
        Me._store = store_

        _dt_Data = New DataTable("Data")

        Dim r_ As DataRow = _dt_Data.NewRow
        _dt_Data.Rows.Add(r_)

        _ds_Main = New DataSet
        _ds_Main.Tables.Add(_dt_Data)

        _ds_ViewManager = _ds_Main.DefaultViewManager
    End Sub

    Public ReadOnly Property dt_Data() As DataTable
        Get
            Return _dt_Data
        End Get
    End Property

    Public ReadOnly Property ds_Main() As DataSet
        Get
            Return _ds_Main
        End Get
    End Property

    Public ReadOnly Property ds_ViewManager() As DataViewManager
        Get
            Return _ds_ViewManager
        End Get
    End Property

    Public ReadOnly Property Fields() As List(Of String)
        Get
            Dim list_ As New List(Of String)
            For Each col_ As DataColumn In _dt_Data.Columns
                list_.Add(col_.ColumnName)
            Next
            Return list_
        End Get
    End Property

    Public Sub AddParam(ByVal name_ As String, ByVal type_ As System.Type)
        _dt_Data.Columns.Add(New DataColumn(name_, type_))
    End Sub

    Public Sub Load() Implements i__DataStore.Load
        _store.Load()
        For Each field_ As String In Me.Fields
            Me.SetValue(field_, _store.Value(field_))
        Next
    End Sub

    Public Sub Save() Implements i__DataStore.Save
        For Each field_ As String In Me.Fields
            _store.SetValue(field_, Me.Value(field_))
        Next
        _store.Save()
    End Sub

    Private Function _ctype(ByVal key_ As String, ByVal value_ As Object) As Object
        Dim t_ As Type = dt_Data.Columns(key_).DataType
        If t_ Is GetType(Boolean) Then
            Return (value_ = True)
        End If
        Return value_
    End Function

    Public Sub SetValue(ByVal key_ As String, ByVal value_ As Object) Implements i__DataStore.SetValue
        Me.dt_Data.Rows(0)(key_) = Me._ctype(key_, value_)
    End Sub

    Public Function Value(ByVal key_ As String) As Object Implements i__DataStore.Value
        Return Me.dt_Data.Rows(0)(key_)
    End Function
End Class

