# README (English) #

( Russian version is below )

The project consist of «Extraction Editor» (XTuner) that could help in preparing an ad-hoc dataset extraction from a relational database into hierarchical XML.

## Technical details

The inner algorithm of the software is using ADO.NET, but externally all that complexities stays effectively hidden from the eyes of regular user. So anyone with intermediate command of SQL can be using the software. One would just open the GUI, put in few SQL queries (with JOINs, WHERE clauses etc) to extract the desired data from DB, and that would be basically it! 

The result of such editing is extraction rules. When the rules are ready the «Editor» allows to:

- Save the resultant rules to a file
- Apply these rules right away to accomplish actual DB data extraction into XML

Aside from this «Extraction Editor» the project provides a standalone DLL that can serve the role of an «extraction engine» that can be used by a third party software. 

Let's say we had extraction rules ready and saved to a file. Then the third party application to accomplish data extraction should simply make a function call against this DLL. This function call should have 3 parameters:

- path to rules file
- path to output (XML) file
- database connection string

# README (Russian) #

Проект включает в себя "Редактор выгрузки" (XTuner), который подготавливает и выполняет выгрузку данных из реляционной базы данных в файлы XML с произвольно заданной иерархической структурой.

## Технические подробности

Внутренний алгоритм программы довольно сложен и использует технологию ADO.NET. Однако программа скрывает от пользователя эти сложности, и потому для ее эффективного использования не требуется высокой квалификации — достаточно лишь уверенного владения SQL. Так, при работе с графическим интерфейсом программы пользователю нужно было лишь ввести в программу ряд SQL запросов к БД, указать строку соединения с БД. Всю интеллектуальную работу программа проделывает сама.

Когда правила выгрузки готовы, программа позволяет:

- сохранить настройки выгрузки в файл
- выполнить выгрузку данных из БД

Кроме программы "Редактор" проект также предоставляет специальную программную компоненту DLL, которая позволяет использовать полученный файл с правилами выгрузки во внешних программах. Для этого внешняя программа должна была лишь осуществить простой вызов функции данной DLL с передачей трех строковых параметров:

- путь к файлу с правилами
- путь к XML файлу выгрузки
- строка соединения с БД