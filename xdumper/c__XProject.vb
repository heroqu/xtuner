﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.XML
Imports System.Text
'Imports xdumper.TableParams

Public Class c__XProject

#Region " Members "

    Private _conn_str As String

    Private _ds_XProject As DataSet

    Private _dt_XMeta As DataTable ' holds project metadata - its name, visibility tunes etc
    Private _dt_XTables As DataTable
    Private _dt_XColumns As DataTable

    Private _ds_ViewManager As DataViewManager

#End Region

#Region " Constructors "

    Public Sub New()
    End Sub

    Public Sub New(ByVal conn_str_ As String)
        ConnectionString = conn_str_
    End Sub


    Private Sub _ini_dataset()

        _ds_XProject = New DataSet("XProject")

        ' table XMeta

        _dt_XMeta = New DataTable("XMeta")
        With _dt_XMeta.Columns
            .Add(New DataColumn("ID", Type.GetType("System.Int32")))
            .Add(New DataColumn("Name", Type.GetType("System.String")))
            .Add(New DataColumn("Description", Type.GetType("System.String")))
            .Add(New DataColumn("GUID", Type.GetType("System.String")))
            .Add(New DataColumn("CreatedAt", Type.GetType("System.DateTime")))
            .Add(New DataColumn("UpdatedAt", Type.GetType("System.DateTime")))
        End With
        _dt_XMeta.PrimaryKey = New DataColumn() {_dt_XMeta.Columns("ID")}
        _ds_XProject.Tables.Add(_dt_XMeta)

        Dim r_ As DataRow = _dt_XMeta.NewRow
        r_("ID") = 1
        r_("Name") = ""
        r_("Description") = ""
        r_("GUID") = Guid.NewGuid.ToString
        r_("CreatedAt") = DateTime.Now
        r_("UpdatedAt") = DateTime.Now
        _dt_XMeta.Rows.Add(r_)


        ' table XTables

        _dt_XTables = New DataTable("XTables")
        With _dt_XTables.Columns
            .Add(New DataColumn("Yes", Type.GetType("System.Boolean")))
            .Add(New DataColumn("TableName", Type.GetType("System.String")))
            .Add(New DataColumn("Condition", Type.GetType("System.String")))
            .Add(New DataColumn("TableID", Type.GetType("System.Int64")))
        End With
        _dt_XTables.PrimaryKey = New DataColumn() {_dt_XTables.Columns("TableID")}
        _ds_XProject.Tables.Add(_dt_XTables)


        ' table XColumns

        _dt_XColumns = New DataTable("XColumns")
        With _dt_XColumns.Columns
            .Add(New DataColumn("ColumnID", Type.GetType("System.Int64")))
            .Add(New DataColumn("Yes", Type.GetType("System.Boolean")))
            .Add(New DataColumn("ColumnName", Type.GetType("System.String")))
            .Add(New DataColumn("ColumnCaption", Type.GetType("System.String")))
            .Add(New DataColumn("TypeName", Type.GetType("System.String")))
            .Add(New DataColumn("MaxLength", Type.GetType("System.Int32")))
            .Add(New DataColumn("IsNullable", Type.GetType("System.Boolean")))
            .Add(New DataColumn("TableID", Type.GetType("System.Int64")))
        End With
        _dt_XColumns.PrimaryKey = New DataColumn() {_dt_XColumns.Columns("TableID"), _dt_XColumns.Columns("ColumnID")}

        _ds_XProject.Tables.Add(_dt_XColumns)

        ' Establish the Relationship "relXTableXColumn"
        ' between XTables ---< XColumns
        _ds_XProject.EnforceConstraints = True

        Dim relXTableXColumn As DataRelation
        Dim colMaster As DataColumn
        Dim colDetail As DataColumn

        colMaster = _ds_XProject.Tables("XTables").Columns("TableID")
        colDetail = _ds_XProject.Tables("XColumns").Columns("TableID")

        relXTableXColumn = New System.Data.DataRelation("relXTableXColumn", colMaster, colDetail)
        _ds_XProject.Relations.Add(relXTableXColumn)

        _ds_ViewManager = _ds_XProject.DefaultViewManager

    End Sub

    ' Call it only after setting the connection string!
    Private Sub _load_tables_and_columns_from_db()

        Dim _conn As New SqlConnection(_conn_str)

        ' Fill the Dataset with XTables, map Default Tablename
        ' "Table" to "XTables".
        Dim _XTables_Query As String = _
             "SELECT                 " & _
             "	0 Yes,               " & _
             "	[name] TableName,    " & _
             "	'' Condition,        " & _
             "	[object_id] TableID  " & _
             "FROM                   " & _
             "	sys.objects          " & _
             "WHERE                  " & _
             "	type = 'U'           " & _
             "ORDER BY               " & _
             "	[name]               "

        Dim da_Tables As New SqlDataAdapter(_XTables_Query, _conn)
        da_Tables.TableMappings.Add("Table", "XTables")
        da_Tables.Fill(_ds_XProject)


        ' Fill the Dataset with XColumns, map Default Tablename
        ' "Table" to "XColumns".
        Dim _XColumns_Query As String = _
           "SELECT                             " & _
           "	c.[object_id] TableID,         " & _
           "	0 Yes,                         " & _
           "	c.[column_id] ColumnID,        " & _
           "	c.[name] ColumnName,           " & _
           "	'' ColumnCaption,              " & _
           "	t.name TypeName,               " & _
           "	c.[max_length] MaxLength,      " & _
           "	c.[is_nullable] IsNullable     " & _
           "FROM                               " & _
           "	sys.objects o                  " & _
           "JOIN                               " & _
           "	sys.columns c                  " & _
           "ON                                 " & _
           "	o.type = 'U'                   " & _
           "AND                                " & _
           "	o.[object_id] = c.[object_id]  " & _
           "JOIN                               " & _
           "	sys.systypes t                 " & _
           "ON                                 " & _
           "	t.[xtype] = c.[system_type_id] " & _
           "AND                                " & _
           "	t.[name] <> 'sysname'          " & _
           "ORDER BY                           " & _
           "	o.name, c.column_id            "

        Dim da_Columns As New SqlDataAdapter(_XColumns_Query, _conn)
        da_Columns.TableMappings.Add("Table", "XColumns")
        da_Columns.Fill(_ds_XProject)

    End Sub


#End Region

#Region " Public Properties "

    Public Property ConnectionString() As String
        Get
            Return _conn_str
        End Get
        Set(ByVal value As String)
            _conn_str = value
            _ini_dataset()
            _load_tables_and_columns_from_db()
        End Set
    End Property

    Public ReadOnly Property ds_XProject() As DataSet
        Get
            Return _ds_XProject
        End Get
    End Property

    Public ReadOnly Property dt_XMeta() As DataTable
        Get
            Return _dt_XMeta
        End Get
    End Property

    Public ReadOnly Property dt_XTables() As DataTable
        Get
            Return _dt_XTables
        End Get
    End Property

    Public ReadOnly Property dt_XColumns() As DataTable
        Get
            Return _dt_XColumns
        End Get
    End Property

    Public ReadOnly Property ds_ViewManager() As DataViewManager
        Get
            Return _ds_ViewManager
        End Get
    End Property

    ' project name
    Public ReadOnly Property Name() As String
        Get
            Try
                Return _dt_XMeta.Rows(0)("Name")
            Catch ex As Exception
                Return ""
            End Try
        End Get
    End Property

#End Region

#Region " Public methods "

    Friend Function GetTableParams_ActiveList() As List(Of TableParams)

        Dim list_ As New List(Of TableParams)

        Dim rr_ As DataRow() = dt_XTables.Select("Yes = true")
        Dim tp_ As TableParams

        For Each r_ As DataRow In rr_
            tp_ = New TableParams
            With tp_
                .TableName = r_("TableName")
                .TableID = CType(r_("TableID"), Int64)
                .Query = GetQueryForTable(.TableID, False)
                .QueryWithCondition = GetQueryForTable(.TableID)
            End With
            list_.Add(tp_)
        Next

        Return list_
    End Function


    Public Function GetEmptyDatasetForDump() As DataSet

        Dim list_ As List(Of TableParams) = GetTableParams_ActiveList()

        Dim da_ As SqlDataAdapter
        Dim dt_ As DataTable
        Dim ds_ As New DataSet("XDump")

        For Each tp_ As TableParams In list_
            da_ = New SqlDataAdapter(tp_.Query, Me.ConnectionString)
            dt_ = New DataTable(tp_.TableName)
            da_.FillSchema(dt_, SchemaType.Mapped)
            ds_.Tables.Add(dt_)
        Next

        Return ds_

    End Function

    Public Function GetAllQueries() As List(Of String)
        Dim list_ As New List(Of String)

        Dim rr_ As DataRow() = dt_XTables.Select("Yes = true")
        Dim query_ As String

        For Each r_ As DataRow In rr_
            query_ = GetQueryForTable(CType(r_("TableID"), Int64))
            If Not String.IsNullOrEmpty(query_) Then
                list_.Add(query_)
            End If
        Next

        Return list_
    End Function

    Public Function GetQueryForTable( _
            ByVal table_id_ As Int64, _
            Optional ByVal with_condition_ As Boolean = True _
            ) As String

        ' Columns

        Dim filter_ As String

        filter_ = String.Format("Yes = true AND TableID = {0}", table_id_)

        Dim rr_ As DataRow() = dt_XColumns.Select(filter_)

        If rr_.Length = 0 Then
            Return Nothing
        End If

        Dim sb_ As New StringBuilder

        sb_.Append("SELECT")

        Dim delimeter_ As String = " "
        Dim r_ As DataRow
        Dim caption_ As Object

        For Each r_ In rr_

            sb_.Append(delimeter_)
            sb_.AppendLine()
            sb_.Append(ControlChars.Tab)

            sb_.Append("[")
            sb_.Append(r_("ColumnName"))
            sb_.Append("]")

            caption_ = r_("ColumnCaption")
            If caption_.GetType Is GetType(String) _
                    AndAlso Not String.IsNullOrEmpty(caption_) _
                    Then
                sb_.Append(" AS ")
                sb_.Append("[")
                sb_.Append(caption_)
                sb_.Append("]")
            End If

            delimeter_ = ", "
        Next

        sb_.AppendLine()
        sb_.AppendLine("FROM ")
        sb_.Append(ControlChars.Tab)

        ' Table & SQL condition

        filter_ = String.Format("TableID = {0}", table_id_)
        rr_ = dt_XTables.Select(filter_)
        If rr_.Length > 0 Then
            r_ = rr_(0)

            ' Table name
            sb_.Append("[")
            sb_.Append(r_("TableName"))
            sb_.AppendLine("] ")

            If with_condition_ Then
                Dim condition_ As String = r_("Condition")
                condition_ = condition_.Trim
                If Not String.IsNullOrEmpty(condition_) Then
                    ' some condition is there
                    sb_.AppendLine("WHERE ")
                    sb_.Append(ControlChars.Tab)
                    sb_.AppendLine(condition_)
                End If
            End If

        End If

        Return sb_.ToString
    End Function

    'Public Sub ReadXml(ByVal xml_reader_ As XmlReader)
    '    Dim ds_ As DataSet = ds_XProject.Clone
    '    ds_.ReadXml(xml_reader_)
    '    _ReadDataSet(ds_)
    'End Sub

    'Public Sub ReadXml(ByVal stream_ As Stream)
    '    Dim ds_ As DataSet = ds_XProject.Clone
    '    ds_.ReadXml(stream_)
    '    _ReadDataSet(ds_)
    'End Sub

    Public Sub ReadXml(ByVal source_ As Object)
        Dim ds_ As DataSet = ds_XProject.Clone
        ds_.ReadXml(source_)
        _ReadDataSet(ds_)
    End Sub

    'Public Sub WriteXml(ByVal xml_writer_ As XmlWriter)
    '    _UpdateTimeStamp()
    '    Dim ds_ As DataSet = _DataSet_for_saving()
    '    ds_.WriteXml(xml_writer_)
    'End Sub

    'Public Sub WriteXml(ByVal stream_ As Stream)
    '    _UpdateTimeStamp()
    '    Dim ds_ As DataSet = _DataSet_for_saving()
    '    ds_.WriteXml(stream_)
    'End Sub

    Public Sub WriteXml(ByVal output_ As Object)
        _UpdateTimeStamp()
        Dim ds_ As DataSet = _DataSet_for_saving()
        ds_.WriteXml(output_)
    End Sub


#End Region

#Region " Private methods "

    Private Sub _ReadDataSet(ByVal ds_ As DataSet)
        _ini_dataset()
        _load_tables_and_columns_from_db()
        ds_XProject.Merge(ds_)
        '_MergeDataset(ds_)
    End Sub

    Private Sub _UpdateTimeStamp()
        If ds_XProject.HasChanges Then
            dt_XMeta.Rows(0)("UpdatedAt") = DateTime.Now
            ds_XProject.AcceptChanges()
        End If
    End Sub

    Private Function _HundredPudsCopy(ByVal ds_original_ As DataSet) As DataSet
        Dim sb_ As New System.Text.StringBuilder
        Dim sw_ As New System.IO.StringWriter(sb_)
        ds_original_.WriteXml(sw_)

        Dim ds_ As New DataSet

        Dim sr_ As New System.IO.StringReader(sb_.ToString)
        ds_.ReadXml(sr_)

        Return ds_
    End Function

    Private Function _DataSet_for_saving() As DataSet
        Dim ds_ As DataSet = _HundredPudsCopy(_ds_XProject)

        Dim dt_ As DataTable = ds_.Tables("XTables")
        Dim dt2_ As DataTable = ds_.Tables("XColumns")

        Dim rr_ As DataRow() = dt_.Select("Yes = false")
        Dim table_id_ As String

        For Each r_ As DataRow In rr_
            table_id_ = r_("TableID")
            _remove_rows_with_table_id(table_id_, dt_)
            _remove_rows_with_table_id(table_id_, dt2_)
        Next

        Return ds_
    End Function

    Private Sub _remove_rows_with_table_id(ByVal table_id_ As String, _
            ByVal dt_ As DataTable)

        Dim filter_ As String = String.Format("TableID = '{0}'", table_id_)

        Dim rr_ As DataRow() = dt_.Select(filter_)

        For Each r_ As DataRow In rr_
            dt_.Rows.Remove(r_)
        Next
    End Sub

    'Private Sub _MergeDataset(ByVal ds_ As DataSet)
    '    For ix_ As Integer = 0 To ds_.Tables.Count - 1
    '        _MergeTable(ds_, ix_)
    '    Next
    'End Sub

    'Private Sub _MergeTable(ByVal ds_ As DataSet, ByVal ix_ As Integer)
    '    Dim dt2_ As DataTable = ds_.Tables(ix_)
    '    Dim dt_ As DataTable = ds_XProject.Tables(ix_)

    '    Dim pk_name_ As String = dt_.PrimaryKey(0).ColumnName

    '    Dim pk_value_ As Integer
    '    Dim filter_ As String
    '    Dim rr_ As DataRow()
    '    Dim r_ As DataRow

    '    Dim col_names_ As New List(Of String)

    '    For Each c_ As DataColumn In dt2_.Columns
    '        col_names_.Add(c_.ColumnName)
    '    Next

    '    For Each r2_ As DataRow In dt2_.Rows
    '        pk_value_ = r2_(pk_name_)
    '        filter_ = String.Format("{0} = {1}", pk_name_, pk_value_)
    '        rr_ = dt_.Select(filter_)
    '        If rr_.Length > 0 Then
    '            r_ = rr_(0)
    '        Else
    '            r_ = dt_.NewRow
    '        End If
    '        For Each col_name_ As String In col_names_
    '            r_(col_name_) = r2_(col_name_)
    '        Next
    '    Next

    'End Sub

#End Region

    Public Shared Sub Dump( _
        ByVal conn_str_ As String, _
        ByVal project_file_ As String, _
        ByVal dump_file_ As String _
        )

        Dim p_ As New c__XProject(conn_str_)
        p_.ReadXml(project_file_)

        Dim list_ As List(Of TableParams) = p_.GetTableParams_ActiveList()

        Dim da_ As SqlDataAdapter
        Dim ds_ As DataSet = p_.GetEmptyDatasetForDump

        For Each tp_ As TableParams In list_
            da_ = New SqlDataAdapter(tp_.QueryWithCondition, p_.ConnectionString)
            da_.Fill(ds_.Tables(tp_.TableName))
        Next

        ds_.WriteXml(dump_file_)

    End Sub


End Class
